# AI BOT FOR Wumpus World


We model the Wumpus world by randomly placing Gold, Pits and Wumpus monster in a walled 5*5 Grid.
The challenge is to design a bot that would complete the game that is collect the Gold without
falling into the pit or being eater by the wumpus monster. As the game progresses the bot adds
new information about surrounding cells to his knowledge base.

The agent uses the probe function to obtain knowledge of the environment around, If there is no threat like a pit or Wumpous for the bot in the current cell
The cell is marked safe.
The Bot constructs the world by marking probable locations of the pits and wompous and the bordering wall.
The complexity we are adding to the traditional problem is the presence of additional arrows that can be collected by the bot.

Two separate knowledge bases are maintained by the bot, one keeps a track of the stench that is observed all over the environment, second keeps a track
of all the breeze that the agent may encounter corresponding to the pit.

The sensors of the bots are :
STENCH
BREEZE
GLITTER

Other sensors are SCREAM When the Wumpus is killed by the arrow And Gold When it finds a cell with Gold.

The States that are maintained are :
Visited Cells
Safe Cells
Cells marked as S or B which are unsafe
Number of arrows Remaining
Array of Moves Made
If Wumpus is Killed.