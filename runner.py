'''
Author - Shubhangi Kishore, Vijay Nandwani, Pallavi Gupta, Shubhang Arora

AI BOT FOR Wumpus World

Driver Program that makes use of agent.py and world1.py where the classes are defined

Readme describes in full the functioning of the program


'''


import agent
import wump_board

print (wump_board.world1.sim(agent.bot()))
print ("You have found the Hidden GOLD Mine ")
