import wumpus
from random import *

width = 6

blocks = set()

for x in range(width+1):
    blocks.add((0, x))
    blocks.add((x, 0))
    blocks.add((width,x))
    blocks.add((x, width))

list = []
for i in range (1,width):
    list.append(i)

# randomly choose a location to place GOLD
x = sample(list, 2)
y = tuple(x)
gold = {y}
# randomly choose a location to place the PIT
x = sample(list, 2)
y = tuple(x)
pits = {y}
# randomly choose a location to place the Wumpus Monster
x = sample(list, 2)
y = tuple(x)
wumpus_location = {y}
# randomly choose a location to place the Bonus Arrows
x = sample(list, 2)
y = tuple(x)
arrow_location = {y}

# initial location of the bot.
initial_location = (1,1)

world1 = wumpus.wumpus_world(blocks = blocks, gold = gold, wumpus = wumpus_location, pits = pits, initial_location = initial_location, arrow_location= arrow_location)

